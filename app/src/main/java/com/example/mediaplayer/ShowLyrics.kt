package com.example.mediaplayer

import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.lyrics.*
import java.io.InputStream
import java.lang.Exception

class ShowLyrics: AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.lyrics)

        var musicIndex = intent.getIntExtra("musicIndex", 0)


        var text = ""
        var inStream: InputStream? = null
        try {
            if (musicIndex == 0){
                inStream = assets.open("saveyourtears.txt")
                lyricsTitle.text = "Save Your Tears\nby\nThe Weeknd ft Ariana Grande"
            }
            if (musicIndex == 1){
                inStream = assets.open("ransom.txt")
                lyricsTitle.text = "Ransom\nby\nSaint JHN"
            }
            if (musicIndex == 2){
                inStream = assets.open("likeiwantyou.txt")
                lyricsTitle.text = "Like I Want You\nby\nGiveon"
            }
            if(musicIndex == 3){
                inStream = assets.open("gooddays.txt")
                lyricsTitle.text = "Good Days\nby\nSZA"
            }
            if(musicIndex == 4){
                inStream = assets.open("heartbreakanniversarry.txt")
                lyricsTitle.text = "Heartbreak Anniversary\nby\nGiveon"
            }
            var buffer = inStream!!.readBytes()
            inStream.read(buffer)
            inStream.close()
            text = String(buffer)
        }
        catch (ex: Exception){
            ex.printStackTrace()
        }
        lyricsText.text = text
    }
}