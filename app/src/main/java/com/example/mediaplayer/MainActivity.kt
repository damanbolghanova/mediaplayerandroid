package com.example.mediaplayer

import android.annotation.SuppressLint
import android.app.*
import android.content.Context
import android.content.Intent
import android.media.AudioManager
import android.media.MediaPlayer
import android.os.*
import android.view.View
import android.widget.SeekBar
import android.widget.TimePicker
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import kotlinx.android.synthetic.main.activity_main.*
import java.lang.Thread.interrupted
import java.lang.Thread.sleep
import java.text.SimpleDateFormat
import java.time.LocalTime
import java.time.format.DateTimeFormatter
import java.util.*

class MainActivity : AppCompatActivity(){
    val CHANNEL_1_ID = "channel1"

    var timePicker: TimePicker? = null
    var pendingIntent: PendingIntent? = null
    var alarmManager: AlarmManager? = null
    var notificationManager: NotificationManagerCompat? = null

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        notificationManager = NotificationManagerCompat.from(this)


        btn.setOnClickListener {
            val cal = Calendar.getInstance()
            val timeSetListener = TimePickerDialog.OnTimeSetListener {
                timePicker, hour, minute ->
                cal.set(Calendar.HOUR_OF_DAY, hour)
                cal.set(Calendar.MINUTE, minute)
                cal.set(Calendar.SECOND, 0)


                updateText.text = SimpleDateFormat("HH:mm").format(cal.time)

                //get current time:
                var currentTime = ""
                Thread( Runnable {
                    run(){
                            try {
                                while (!interrupted()){
                                    sleep(1000)
                                    runOnUiThread {
                                        currentTime = LocalTime.now().format(DateTimeFormatter.ofPattern("HH:mm")).toString()

                                        val alarmTime = updateText.text

                                        if (currentTime==alarmTime){
                                            sendNotification("You set time to $alarmTime. Time is up now!")
                                        }
                                    }
                                }
                            } catch (e: InterruptedException) {
                                e.printStackTrace()
                            }
                        }
                }).start()
                /*val currentTime = LocalTime.now().format(DateTimeFormatter.ofPattern("HH:mm")).toString()
                println(currentTime)*/


                var intent = Intent(this, com.example.mediaplayer.MediaPlayer::class.java)
                startActivity(intent)

            }
            val hour = cal.get(Calendar.HOUR_OF_DAY)
            val minute = cal.get(Calendar.MINUTE)
            TimePickerDialog(this, timeSetListener,
                    hour, minute,
                    true).show()
        }
    }

    private fun sendNotification(text: String){
       if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel1 = NotificationChannel(
                    CHANNEL_1_ID,
                    "Channel 1",
                    NotificationManager.IMPORTANCE_HIGH
            )
            channel1.description = "This is channel 1"

            val manager = this.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            manager.createNotificationChannel(channel1)

        }
        val notification: Notification = NotificationCompat.Builder(this, CHANNEL_1_ID)
                .setSmallIcon(R.drawable.ic_notify)
                .setContentTitle("Media Player")
                .setContentText(text)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setCategory(NotificationCompat.CATEGORY_MESSAGE)
                .build()

        notificationManager?.notify(1, notification)
        }
}

