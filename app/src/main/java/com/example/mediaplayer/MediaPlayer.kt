package com.example.mediaplayer

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.media.AudioManager
import android.media.MediaPlayer
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.os.Message
import android.view.View
import android.widget.SeekBar
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.media_player.*

class MediaPlayer: AppCompatActivity() {
    private var mMediaPlayer: MediaPlayer? = null
    private var mAudioManager: AudioManager? = null
    private var runnable: Runnable? = null
    private var currentIndex = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.media_player)

        mAudioManager = getSystemService(Context.AUDIO_SERVICE) as AudioManager

        var songs: ArrayList<Int> = ArrayList()
        songs.add(0, R.raw.saveyourtears)
        songs.add(1, R.raw.ransom)
        songs.add(2, R.raw.likeiwantyou)
        songs.add(3, R.raw.gooddays)
        songs.add(4, R.raw.heartbreakanniversary)

        var intent = Intent(this, ShowLyrics::class.java)
        showLyrics.setOnClickListener {
            startActivity(intent)

        }

        //initializing mediaplayer
        mMediaPlayer = MediaPlayer.create(applicationContext, songs.get(currentIndex))

        //seekBar Volume
        var maxV = mAudioManager!!.getStreamMaxVolume(AudioManager.STREAM_MUSIC)
        var curV = mAudioManager!!.getStreamVolume(AudioManager.STREAM_MUSIC)
        seekBarVol.max = maxV
        seekBarVol.progress = curV

        seekBarVol.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener{
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                mAudioManager!!.setStreamVolume(AudioManager.STREAM_MUSIC, progress, 0)
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {
            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
            }
        })

        play.setOnClickListener( View.OnClickListener {
            seekBarTime.max = mMediaPlayer!!.duration
            if (mMediaPlayer != null && mMediaPlayer!!.isPlaying){
                mMediaPlayer!!.pause()
                play!!.setImageResource(R.drawable.play_btn)
            } else{
                mMediaPlayer!!.start()
                play!!.setImageResource(R.drawable.pause_btn)
            }
            songNames()
        })

        next.setOnClickListener( View.OnClickListener{
            //seekBarTime.max = mMediaPlayer!!.duration
            if (mMediaPlayer != null){
                play!!.setImageResource(R.drawable.pause_btn)
            }
            if (currentIndex < songs.size -1){
                currentIndex++
                intent.putExtra("musicIndex", currentIndex)
            }
            else{
                currentIndex = 0
            }
            if (mMediaPlayer!!.isPlaying){
                mMediaPlayer!!.stop()
            }

            mMediaPlayer = MediaPlayer.create(applicationContext, songs.get(currentIndex))
            mMediaPlayer!!.start()
            songNames()
        })

        prev.setOnClickListener( View.OnClickListener {
            if (mMediaPlayer != null){
                play!!.setImageResource(R.drawable.pause_btn)
            }
            if (currentIndex > 0){
                currentIndex--
                intent.putExtra("musicIndex", currentIndex)
            } else{
                currentIndex = songs.size-1
            }
            if (mMediaPlayer!!.isPlaying){
                mMediaPlayer!!.stop()
            }

            mMediaPlayer = MediaPlayer.create(applicationContext, songs.get(currentIndex))
            mMediaPlayer!!.start()
            songNames()
        })

        Thread( Runnable {
            run(){
                while (mMediaPlayer != null) {
                    try {
                        if (mMediaPlayer!!.isPlaying) {
                            val message = Message()
                            message.what = mMediaPlayer!!.currentPosition
                            mHandler.sendMessage(message)
                            Thread.sleep(1000)
                        }
                    } catch (e: InterruptedException) {
                        e.printStackTrace()
                    }
                }

            }
        }).start()
    }


    private var mHandler = @SuppressLint("HandlerLeak")
    object: Handler(Looper.getMainLooper()){
        override fun handleMessage(msg: Message) {
            seekBarTime.setProgress(msg.what)
        }
    }

    private fun songNames(){
        if (currentIndex == 0){
            songTitle.text = "Save Your Tears - The Weeknd, Ariana Grande"
            imageView!!.setImageResource(R.drawable.saveyourtears)
        }
        if (currentIndex == 1){
            songTitle.text = "Ransom - Saint JHN"
            imageView!!.setImageResource(R.drawable.ransom)
        }
        if (currentIndex == 2){
            songTitle.text = "Like I Want You - Giveon"
            imageView!!.setImageResource(R.drawable.likeiwantyou)
        }
        if (currentIndex == 3){
            songTitle.text = "Good Days - SZA"
            imageView!!.setImageResource(R.drawable.gooddays)
        }
        if (currentIndex == 4){
            songTitle.text = "Heartbreak Anniversary - Giveon"
            imageView!!.setImageResource(R.drawable.heartbreakaniversary)
        }

        //seekbar duration
        mMediaPlayer!!.setOnPreparedListener( object : MediaPlayer.OnPreparedListener{
            override fun onPrepared(mp: MediaPlayer?) {
                seekBarTime.max = mMediaPlayer!!.duration
                mMediaPlayer!!.start()
            }
        })




        seekBarTime.setOnSeekBarChangeListener( object : SeekBar.OnSeekBarChangeListener{
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                if (fromUser){
                    mMediaPlayer!!.seekTo(progress)
                    seekBarTime.setProgress(progress)
                    /*mMediaPlayer!!.seekTo(progress)
                    seekBarTime.progress = progress*/
                    /*seekBarTime.setProgress(progress)
                    mMediaPlayer!!.seekTo(progress)*/
                }
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {
            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
            }

        })

    }
}